//
//  game.hpp
//  MinesweeperTwist
//
//  Created by Tine Ramos on 16/12/2015.
//  Copyright © 2015 Tine Ramos. All rights reserved.
//

#ifndef game_hpp
#define game_hpp

#include <stdio.h>

#include <SFML/Audio.hpp>
#include <SFML/Graphics.hpp>

// Here is a small helper for you ! Have a look.
#include "ResourcePath.hpp"

#include "minefieldMap.hpp"

class Game {

public:
    Game();
    Game(const Game&);
    ~Game();
    
    Game& operator=(const Game&);
    
    void run();
    void menu();
    void play();
    
private:
    bool isPlaying;
    bool gamePause;
    bool finishPlaying;
    int result;    // 0 - lose, 1 - win
    
    void processEvents();   // responsible for user input, polls app window for any input events
    void update(sf::Time);  // updates the game
    void render();          // renders the update
    
    void handlePlayerInput(sf::Keyboard::Key, bool);

    void handleMousePressedEvent(sf::Event);
    void handleMouseReleasedEvent(sf::Event);
    
    sf::Vector2i convertToMapCoordinate(int, int);
    
private:
    sf::RenderWindow window;

    sf::Font font;
    
    // used for menu
    sf::Text titleText;
    sf::Text startText;
    sf::Texture logoTexture;
    
    sf::Text backText;
    
    // used for play
    MinefieldMap *mineField;
    sf::Texture tileTexture;
    
    bool didWin(int, int);
    bool didLose(int, int);
    
    void copy(const Game&);
    
};

#endif /* game_hpp */
