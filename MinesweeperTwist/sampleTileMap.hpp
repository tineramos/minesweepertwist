//
//  sampleTileMap.hpp
//  MinesweeperTwist
//
//  Created by Tine Ramos on 16/12/2015.
//  Copyright © 2015 Tine Ramos. All rights reserved.
//

#ifndef sampleTileMap_hpp
#define sampleTileMap_hpp

#include <stdio.h>

#include "SFML/Graphics.hpp"

class TileMap : public sf::Drawable, public sf::Transformable
{
    
public:
    TileMap();
    
private:
    virtual void draw(sf::RenderTarget& target, sf::RenderStates states) const
    {
        // apply the transform
        states.transform *= getTransform();
        
        // apply the tileset texture
        states.texture = &m_tileset;
        
        // draw the vertex array
        target.draw(m_vertices, states);
    }
    
    sf::VertexArray m_vertices;
    sf::Texture m_tileset;
//    sf::Vector2u m_tileSize;
    
    int tiles[];
    void createMinefieldMap();
};

#endif /* sampleTileMap_hpp */
