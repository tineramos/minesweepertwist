//
//  gameMap.cpp
//  MinesweeperTwist
//
//  Created by Tine Ramos on 16/12/2015.
//  Copyright © 2015 Tine Ramos. All rights reserved.
//
//  Acknowledgement: https://github.com/gshopov/SFML-Minesweeper
//

#include "minefieldMap.hpp"

#include "ResourcePath.hpp"

#include <iostream>
#include <stdio.h>

using namespace std;

const int mines     = 10;
const int width     = 12;
const int height    = 12;
const int tile_height = 25;

MinefieldMap::MinefieldMap(const sf::Texture &texture)
{
    unsigned int time_ui = unsigned(time(0));
    srand(time_ui);
    
    unsigned int texture_height = texture.getSize().y;
    
    // load, trim and save tile images from the tileset in an array
    for (int i = 0; i < 15; i++) {
        tile_textures[i].loadFromFile(resourcePath() + "tileset.png",
                                      sf::IntRect(i * texture_height, 0, texture_height, texture_height));
    }
    
    // create the map tile with initial value of 0
    field = new MinefieldTile**[height];
    for (int i = 0; i < height; i++) {
        field[i] = new MinefieldTile*[width];
        for (int j = 0; j < width; j++) {
            field[i][j] = new MinefieldTile(tile_textures[0], 0, j*tile_height, i*tile_height, CellStatusClose);
        }
    }
    
    // WARNING: don't interchange position of function call
    plantMines();
    addBase();
    hideTrigger();
}

void MinefieldMap::selectTile(int x, int y)
{
    // can only select close tile
    if (field[x][y][0].getCellStatus() == CellStatusClose) {
        field[x][y][0].setTexture(tile_textures[1]);
        cout << "BEH: Cell is now selected" << endl;
    }
}

void MinefieldMap::openTile(int x, int y)
{
    // can only open close tile
    if (field[x][y][0].getCellStatus() == CellStatusClose) {
        field[x][y][0].setCellStatus(CellStatusOpen);
        
        int value = field[x][y][0].getValue();
        cout << "The value is " << value << endl;
        
        if (value >= 13) {
            cout << "Base or trigger opened" << endl;
            field[x][y][0].setTexture(tile_textures[value]);
        }
        else {
            // added four to adjust relative to tileset image position
            cout << "Meh, maybe bomb or nothing" << endl;
            field[x][y][0].setTexture(tile_textures[value + 4]);
        }
        
    }
}

void MinefieldMap::flagUnflagTile(int x, int y)
{
    if (field[x][y][0].getCellStatus() == CellStatusOpen) {
//        cout << "BEH: Flagging an open cell" << endl;
        return;
    }
    else {
        if (field[x][y][0].getCellStatus() == CellStatusClose) {
            field[x][y][0].setCellStatus(CellStatusFlag);
            field[x][y][0].setTexture(tile_textures[2]);
//            cout << "BEH: Cell is now flagged" << endl;
        }
        else {
            field[x][y][0].setCellStatus(CellStatusClose);
            field[x][y][0].setTexture(tile_textures[0]);
//            cout << "BEH: Cell is now closed" << endl;
        }
    }
}

void MinefieldMap::revealHintTiles(int x, int y)
{
    cout << "Blank/Number tile is opened. Hint tiles shall be opened." << endl;
    
    for (int i = x-1; i <= x+1; i++) {
        
        if (i < 0 || i >= width) {
            continue;
        }
        
        for (int j = y-1; j <= y+1; j++) {
            
            if (j < 0 || j >= height) {
                continue;
            }
            
            int tileValue = field[i][j][0].getValue();
            
            cout << "To be opened tile. value is: " << tileValue << endl;
            
            if (tileValue >= 0 && tileValue < 13 && field[i][j][0].getCellStatus() == CellStatusClose) {
                openTile(i, j);
            }
        }
    }
}

// show all mines, trigger and base
void MinefieldMap::revealSignificantTiles()
{
    cout << "Reveal significant function start." << endl;
    
    for (int i = 0; i < width; i++) {
        for (int j = 0; j < height; j++) {
            
            int value = field[i][j][0].getValue();
            
            if ((value == -1 || value > 12) && field[i][j][0].getCellStatus() == CellStatusClose) {
                openTile(i, j);
            }
            
        }
    }
    
    cout << "Reveal significant function end." << endl;
}

void MinefieldMap::plantMines()
{
    int minesCounter = 0;
    
    while (minesCounter != 10) {
        
        int randomX = rand() % width;
        int randomY = rand() % height;
        
        if (field[randomX][randomY][0].getValue() != -1) {
            field[randomX][randomY][0].setValue(-1);
            
//            cout << "MINE_X: " << randomX << " MINE_Y: " << randomY << endl;
            calculateTileValuesAroundCoordinate(randomX, randomY);
            
            minesCounter++;
        }

    }
}

void MinefieldMap::addBase()
{
//    cout << "Start of base" << endl;
    
    bool baseIsAdded = false;
    
    while (baseIsAdded == false) {
        
        int randomX = rand() % width;
        int randomY = rand() % height;
        
        if (field[randomX][randomY][0].getValue() != -1) {
            field[randomX][randomY][0].setValue(13);
            
//            cout << "BASE_X: " << randomX << " BASE_Y: " << randomY << endl;
            calculateTileValuesAroundCoordinate(randomX, randomY);
            
            baseIsAdded = true;
        }
        
    }
    
//    cout << "End of base" << endl;
    
}

void MinefieldMap::hideTrigger()
{
//    cout << "Start of trigger" << endl;
    
    bool triggerIsHidden = false;
    
    while (triggerIsHidden == false) {
        
        int randomX = rand() % width;
        int randomY = rand() % height;
        
        int tileValue = field[randomX][randomY][0].getValue();
        
        if (tileValue >= 0 && tileValue != 13) {
            field[randomX][randomY][0].setValue(14);
            
//            cout << "TRIGGER_X: " << randomX << " TRIGGER_Y: " << randomY << endl;
            calculateTileValuesAroundCoordinate(randomX, randomY);
            
            triggerIsHidden = true;
        }
        
    }
    
//    cout << "End of trigger" << endl;
}

// once a mine, trigger or base was set, call this function to calculate the value of surrounding tiles
void MinefieldMap::calculateTileValuesAroundCoordinate(int x, int y)
{
//    cout << "Start of calculation" << endl;
    
    for (int i = x-1; i <= x+1; i++) {
        
        if (i < 0 || i >= width) {
            continue;
        }
        
        for (int j = y-1; j <= y+1; j++) {
            
//            cout << "Calculating... inner y loop" << endl;
            
            if (j < 0 || j >= height) {
                continue;
            }
            
            int tileValue = field[i][j][0].getValue();
//            cout << "The tile value is " << tileValue << endl;
            if (field[i][j][0].getValue() >= 0 && field[i][j][0].getValue() < 13) {
                field[i][j][0].setValue(tileValue+1);
            }
        }
    }
    
//    cout << "End of calculation" << endl;
}

void MinefieldMap::drawField(sf::RenderWindow& window) const
{
    for (int i = 0; i < height; i++) {
        for (int j = 0; j < width; j++) {
            field[i][j][0].drawCell(window);
        }
    }
}

void MinefieldMap::copy(const MinefieldMap &other)
{
    for (int i = 0; i < 15; i++)
        tile_textures[i] = other.tile_textures[i];
    
    field = new MinefieldTile**[height];
    for (int i = 0; i < height; i++) {
        
        field[i] = new MinefieldTile*[width];
        for (int j = 0; j < width; j++)
            field[i][j] = new MinefieldTile(tile_textures[0], other.field[i][j][0].getValue(), j*tile_textures[0].getSize().x,
                                            i*tile_textures[0].getSize().y, CellStatusClose);
    }
}

int MinefieldMap::getCellValue(int x, int y) const
{
    return field[x][y][0].getValue();
}

CellStatus MinefieldMap::getCellStatus(int x, int y) const
{
    return field[x][y][0].getCellStatus();
}

MinefieldMap::MinefieldMap(const MinefieldMap &other)
{
    copy(other);
}

MinefieldMap::~MinefieldMap()
{
    deleteField();
}

MinefieldMap& MinefieldMap::operator=(const MinefieldMap& other)
{
    if (this != &other)
    {
        deleteField();
        copy(other);
    }
    return *this;
}

void MinefieldMap::deleteField()
{
    for (int i = 0; i < width; i++) {
        
        for (int j = 0; j < height; j++) {
            
            delete field[i][j];
            
        }
        
        delete [] field[i];
        
    }
    
    delete [] field;
}
