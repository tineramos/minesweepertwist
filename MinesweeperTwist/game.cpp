//
//  game.cpp
//  MinesweeperTwist
//
//  Created by Tine Ramos on 16/12/2015.
//  Copyright © 2015 Tine Ramos. All rights reserved.
//

#include "game.hpp"

#include <stdio.h>
#include <iostream>

#include <math.h>

using namespace std;

const int size      = 25;
const int length    = 12;

// make the game run 60 frame per second
const sf::Time TimePerFrame = sf::seconds(1.f/60.f);

Game::Game() : window(sf::VideoMode(300, 300, 32), "Minesweeper Twist", sf::Style::Close)
, logoTexture()
, font()
, titleText("MINESWEEPER\nTWIST", font, 35)
, startText("Press space to start", font, 20)
, backText("Press Q to display menu", font, 15)
{
    // Set the App Icon
    sf::Image icon;
    if (!icon.loadFromFile(resourcePath() + "icon.png")) {
        return EXIT_FAILURE;
    }
    window.setIcon(icon.getSize().x, icon.getSize().y, icon.getPixelsPtr());
    
    if (!font.loadFromFile(resourcePath() + "sansation.ttf")) {
        return EXIT_FAILURE;
    }
    
    if (!tileTexture.loadFromFile(resourcePath() + "tileset.png")) {
        return EXIT_FAILURE;
    }
    
    logoTexture.setSmooth(true);
    if (!logoTexture.loadFromFile(resourcePath() + "icon.png", sf::IntRect(50, 50, 200, 200))) {
        return EXIT_FAILURE;
    }
    
    titleText.setColor(sf::Color::Black);
    titleText.setPosition(10, 150);
    
    startText.setColor(sf::Color::Black);
    startText.setPosition(10, 250);
}

void Game::run()
{
    mineField = NULL;
    
    isPlaying = false;
    gamePause = true;
    
    finishPlaying = false;
    result = 0;
    
    window.setVerticalSyncEnabled(true);
    
    while (window.isOpen()) {
        
        processEvents();
        
        sf::Clock clock;
        sf::Time timeSinceLastUpdate = sf::Time::Zero;
        
        timeSinceLastUpdate += clock.restart();
        
        // this loop collects user input and game logic
        while (timeSinceLastUpdate > TimePerFrame) {
            
            timeSinceLastUpdate -= TimePerFrame;
            processEvents();
            update(TimePerFrame);
            
        }
        
        render();
    }
}

void Game::menu()
{
    window.clear(sf::Color::White);
    
    sf::Sprite sprite(logoTexture);
    window.draw(sprite);
    
    // Draw the string
    window.draw(titleText);
    window.draw(startText);
}

void Game::play()
{
    window.clear(sf::Color::White);
    mineField->drawField(window);
}

void Game::processEvents()
{
    sf::Event event;
    while (window.pollEvent(event)) {
        
        switch (event.type) {
            case sf::Event::KeyPressed:
                handlePlayerInput(event.key.code, true);
                break;
                
            case sf::Event::KeyReleased:
                handlePlayerInput(event.key.code, false);
                break;
                
            case sf::Event::MouseButtonPressed:
                handleMousePressedEvent(event);
                break;
                
            case sf::Event::MouseButtonReleased:
                handleMouseReleasedEvent(event);
                break;
                
            default:
                break;
        }
        
        // closes windows/app
        if (event.type == sf::Event::Closed || event.key.code == sf::Keyboard::Escape) {
            window.close();
        }
    }
}

void Game::update(sf::Time)
{
    
}

void Game::render()
{
    if (isPlaying) {
        play();
        
        /*
        if (finishPlaying) {
            
            if (result == 1) {
                // win
                sf::Texture texture;
                texture.setSmooth(true);
                if (!texture.loadFromFile(resourcePath() + "won.png", sf::IntRect(0, 0, 300, 300))) {
                    return EXIT_FAILURE;
                }
                
                sf::Sprite sprite(texture);
                window.draw(sprite);
                
            }
            else {
                // lose
                sf::Texture texture;
                texture.setSmooth(true);
                if (!texture.loadFromFile(resourcePath() + "lose.png",  sf::IntRect(0, 0, 300, 300))) {
                    return EXIT_FAILURE;
                }
                
                sf::Sprite sprite(texture);
                window.draw(sprite);
            }
        }
        */
        
    }
    else {
        menu();
    }
    
    window.display();
}

void Game::handlePlayerInput(sf::Keyboard::Key key, bool isPressed)
{
    if (!isPlaying) {
        if (key == sf::Keyboard::Space) {
            // will display game screen
            isPlaying = true;
            gamePause = false;
            
            mineField = new MinefieldMap(tileTexture);
            
        }
    }
    else {
        
        if (key == sf::Keyboard::Q) {  // quit game
            // will display first screen
            isPlaying = false;
            gamePause = true;
            finishPlaying = false;
            
            delete mineField;
        }
        else {
            if (!gamePause && key == sf::Keyboard::P) {
                gamePause = true;
                window.setTitle("pause game!");
            }
            else if (key == sf::Keyboard::R) {
                gamePause = false;
                window.setTitle("resume game!");
            }
            
            // ignore any other key press
        }
    }
}

void Game::handleMousePressedEvent(sf::Event event)
{
    if (!isPlaying || finishPlaying) {
        return;
    }
    
    // get the mouse position (x,y) here
    sf::Vector2i localPosition = sf::Mouse::getPosition(window);
    sf::Vector2i convertedPosition = convertToMapCoordinate(localPosition.x, localPosition.y);
    
    if (event.mouseButton.button == sf::Mouse::Left) {
//        cout << "LL: X is: " << localPosition.x << " Y is " << localPosition.y << endl;
//        cout << "CL: X is: " << convertedPosition.x << " Y is " << convertedPosition.y << endl;
        
        // add selected cell
        mineField->selectTile(convertedPosition.x, convertedPosition.y);
    }
    else {
        // flag/unflag
        // check if a winning condition is met
        
//        cout << "RR: X is: " << localPosition.x << " Y is " << localPosition.y << endl;
//        cout << "CR: X is: " << convertedPosition.x << " Y is " << convertedPosition.y << endl;
         
        // add unflag method here
        mineField->flagUnflagTile(convertedPosition.x, convertedPosition.y);
        
        if (didWin(convertedPosition.x, convertedPosition.y)) {
            cout << "WINNER!" << endl;
            mineField->revealSignificantTiles();
        }
        
    }
}

void Game::handleMouseReleasedEvent(sf::Event event)
{
    if (!isPlaying || finishPlaying) {
        return;
    }
    
    // get the mouse position (x,y) here
    sf::Vector2i localPosition = sf::Mouse::getPosition(window);
    sf::Vector2i convertedPosition = convertToMapCoordinate(localPosition.x, localPosition.y);
    
    if (event.mouseButton.button == sf::Mouse::Left) {
        // open/reveal cell value
        // check if a winning/losing condition is met
        
//        cout << "LL: X is: " << localPosition.x << " Y is " << localPosition.y << endl;
//        cout << "CL: X is: " << convertedPosition.x << " Y is " << convertedPosition.y << endl;
        
        mineField->openTile(convertedPosition.x, convertedPosition.y);
        
        if (didWin(convertedPosition.x, convertedPosition.y)) {
            cout << "WINNER!" << endl;
            mineField->revealSignificantTiles();
        }
        else if (didLose(convertedPosition.x, convertedPosition.y)) {
            cout << "LOSER!" << endl;
            mineField->revealSignificantTiles();
        }
        else {
            mineField->revealHintTiles(convertedPosition.x, convertedPosition.y);
        }
        
        // close cell
    }
}

sf::Vector2i Game::convertToMapCoordinate(int _x, int _y)
{
//    cout << "PASS: X is: " << _x << " Y is " << _y << endl;
    
    int temp_x = ceil((double)_x / size) - 1;
    int temp_y = ceil((double)_y / size) - 1;
    
//    cout << "TEMP: X is: " << temp_x << " Y is " << temp_y << endl;
    
    if (temp_x < 0 || temp_y < 0) {
        return sf::Vector2i(0, 0);
    }
    return sf::Vector2i(temp_y, temp_x);
    
}

bool Game::didWin(int x, int y)
{
    cout << "DIDWIN HERE?" << endl;
    // opened the base tile
    if (mineField->getCellValue(x, y) == 13 && mineField->getCellStatus(x, y) == CellStatusOpen) {
        finishPlaying = true;
        result = 1;
        return true;
    }
    else {
        // check if all mines has been flagged
        int mineCounter = 0;
        
        cout << "MINECOUNTER HERE?" << endl;
        
        for (int i = 0; i < length; i++) {
            for (int j = 0; j < length; j++) {
//                cout << "MINECOUNTER HERE?" << i << " " << j << endl;
                if (mineField->getCellValue(i, j) == -1 && mineField->getCellStatus(i, j) == CellStatusFlag) {
                    mineCounter++;
                    
                    if (mineCounter == 10) {
                        finishPlaying = true;
                        result = 1;
                        return true;
                    }
                    
                }
            }
        }
        
        cout << "end win?" << endl;
    }
    
    return false;
}

bool Game::didLose(int x, int y)
{
    cout << "DIDLOSE HERE?" << endl;
    
    // check if the player opened the trigger
    if (mineField->getCellValue(x, y) == 14 && mineField->getCellStatus(x, y) == CellStatusOpen) {
        finishPlaying = true;
        result = 0;
        return true;
    }
    else {
        
        // check if all mines has been opened
        int mineCounter = 0;
        
        cout << "MINECOUNTter lose?" << endl;
        
        for (int i = 0; i < length; i++) {
            for (int j = 0; j < length; j++) {
                if (mineField->getCellValue(i, j) == -1 && mineField->getCellStatus(i, j) == CellStatusOpen) {
                    mineCounter++;
                    
                    if (mineCounter == 10) {
                        finishPlaying = true;
                        result = 0;
                        return true;
                    }
                    else {
                        
                        cout << "inner?" << endl;
                        
                        // check if the mine opened is adjacent to the base
                        for (int a = i-1; a <= i+1; a++) {
                            
                            if (a < 0 || a >= length)
                                continue;
                            
                            for (int b = j-1; b <= j+1; b++) {
                                
                                if (j < 0 || j >= length)
                                    continue;
                                else if (mineField->getCellValue(i, j) == 13) {
                                    finishPlaying = true;
                                    result = 0;
                                    return true;
                                }
                            }
                        }
                        
                    }
                    
                }
            }
        }
        
        cout << "end win?" << endl;
        
    }
    
    return false;
}

Game::Game(const Game &other)
{
    copy(other);
}

Game& Game::operator=(const Game& other)
{
    if (this != &other) {
        delete mineField;
        copy(other);
    }
    
    return *this;
}

void Game::copy(const Game &other)
{
    font = other.font;
    tileTexture = other.tileTexture;
    logoTexture = other.logoTexture;
    
    backText = other.backText;
    
    titleText = other.titleText;
    startText = other.startText;
    
    mineField = new MinefieldMap(*other.mineField);
    
    window.create(sf::VideoMode(300, 300, 32), "Minesweeper Twist", sf::Style::Close);
}

Game::~Game()
{
    delete mineField;
}
