//
//  sampleTileMap.cpp
//  MinesweeperTwist
//
//  Created by Tine Ramos on 16/12/2015.
//  Copyright © 2015 Tine Ramos. All rights reserved.
//

#include "sampleTileMap.hpp"

#include "ResourcePath.hpp"

#include <iostream>

const int width     = 12;
const int height    = 16;
const int mines     = 10;
const int base      = 1;
const int trigger   = 1;

TileMap::TileMap()
{
    sf::Vector2u tileSize(32, 32);
    
    // load the tileset texture
    if (!m_tileset.loadFromFile(resourcePath() + "tileset.png")) {
        return EXIT_FAILURE;
    }
    
    // resize the vertex array to fit the level size
    m_vertices.setPrimitiveType(sf::Quads);
    m_vertices.resize(width * height * 4);
    
    createMinefieldMap();
    
    // populate the vertex array, with one quad per tile
    for (unsigned int i = 0; i < width; ++i) {
        for (unsigned int j = 0; j < height; ++j)
        {
            // get the current tile number
            int tileNumber = tiles[i + j * width];
            
            // find its position in the tileset texture
            int tu = tileNumber % (m_tileset.getSize().x / tileSize.x);
            int tv = tileNumber / (m_tileset.getSize().x / tileSize.x);
            
            // get a pointer to the current tile's quad
            sf::Vertex* quad = &m_vertices[(i + j * width) * 4];
            
            // define its 4 corners
            quad[0].position = sf::Vector2f(i * tileSize.x, j * tileSize.y);
            quad[1].position = sf::Vector2f((i + 1) * tileSize.x, j * tileSize.y);
            quad[2].position = sf::Vector2f((i + 1) * tileSize.x, (j + 1) * tileSize.y);
            quad[3].position = sf::Vector2f(i * tileSize.x, (j + 1) * tileSize.y);
            
            // define its 4 texture coordinates
            quad[0].texCoords = sf::Vector2f(tu * tileSize.x, tv * tileSize.y);
            quad[1].texCoords = sf::Vector2f((tu + 1) * tileSize.x, tv * tileSize.y);
            quad[2].texCoords = sf::Vector2f((tu + 1) * tileSize.x, (tv + 1) * tileSize.y);
            quad[3].texCoords = sf::Vector2f(tu * tileSize.x, (tv + 1) * tileSize.y);
        }
    }
}

void TileMap::createMinefieldMap()
{
    unsigned int time_ui = unsigned(time(0));
    srand(time_ui);
    
    int minesCounter = 0;
    
    while (minesCounter != 10) {
        
        int randomIndex = rand() % (width * height);
        
        if (tiles[randomIndex] != 3) {
            tiles[randomIndex] = 3;
            minesCounter++;
        }
        
    }
    
}
