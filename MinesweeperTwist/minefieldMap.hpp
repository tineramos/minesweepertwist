//
//  gameMap.hpp
//  MinesweeperTwist
//
//  Created by Tine Ramos on 16/12/2015.
//  Copyright © 2015 Tine Ramos. All rights reserved.
//
//  Acknowledgement: https://github.com/gshopov/SFML-Minesweeper
//

#ifndef minefieldMap_hpp
#define minefieldMap_hpp

#include "minefieldTile.hpp"

class MinefieldMap
{
    
public:
    MinefieldMap(const sf::Texture &);
    MinefieldMap(const MinefieldMap &);
    ~MinefieldMap();
    
    MinefieldMap& operator = (const MinefieldMap &); // TODO: research on this
    
    // play methods
    void selectTile(int, int);
    void openTile(int, int);
    void flagUnflagTile(int, int);
    void revealHintTiles(int, int);
    void revealSignificantTiles();
    
    int getCellValue(int, int) const;
    CellStatus getCellStatus(int, int) const;
    
    void drawField(sf::RenderWindow&) const;
    
private:
    void plantMines();
    void addBase();
    void hideTrigger();
    void deleteField();
    
    void calculateTileValuesAroundCoordinate(int, int);
    
    MinefieldTile ***field;
    sf::Texture tile_textures[15];  // this will contain tile images
    
    void copy(const MinefieldMap &);
        
};

#endif /* minefieldMap_hpp */
