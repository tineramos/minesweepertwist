//
//  minefieldTile.cpp
//  MinesweeperTwist
//
//  Created by Tine Ramos on 16/12/2015.
//  Copyright © 2015 Tine Ramos. All rights reserved.
//
//  Acknowledgement: https://github.com/gshopov/SFML-Minesweeper
//

#include "minefieldTile.hpp"

MinefieldTile::MinefieldTile(const sf::Texture &texture, int _value, float _x, float _y, CellStatus _cellStatus):
sprite(texture), size(texture.getSize())
{
    value = _value;
    cellStatus = _cellStatus;
    sprite.setScale(sf::Vector2f(0.4, 0.4));
    sprite.setPosition(sf::Vector2f(_x, _y));
}

void MinefieldTile::setTexture(const sf::Texture &_texture)
{
    sprite.setTexture(_texture);
    size = _texture.getSize();
}

void MinefieldTile::setPosition(float _x, float _y)
{
    sprite.setPosition(sf::Vector2f(_x, _y));
}

void MinefieldTile::setValue(int _value)
{
    value = _value;
}

void MinefieldTile::setCellStatus(CellStatus _status)
{
    cellStatus = _status;
}

void MinefieldTile::setScale(float _x, float _y)
{
    sprite.setScale(_x, _y);
}

int MinefieldTile::getValue() const
{
    return value;
}

CellStatus MinefieldTile::getCellStatus() const
{
    return cellStatus;
}

sf::Vector2f MinefieldTile::getPosition() const
{
    return sprite.getPosition();
}

sf::Vector2f MinefieldTile::getScale() const
{
    return sprite.getScale();
}

void MinefieldTile::drawCell(sf::RenderWindow& window) const
{
    window.draw(sprite);
}
