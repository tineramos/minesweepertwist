//
//  minefieldTile.hpp
//  MinesweeperTwist
//
//  Created by Tine Ramos on 16/12/2015.
//  Copyright © 2015 Tine Ramos. All rights reserved.
//
//  Acknowledgement: https://github.com/gshopov/SFML-Minesweeper
//

#ifndef minefieldTile_hpp
#define minefieldTile_hpp

#include <stdio.h>

#include "SFML/Graphics.hpp"

typedef enum : int {
    CellStatusClose = 0,
    CellStatusOpen,
    CellStatusFlag,
} CellStatus;

typedef enum : int {
    CellValueBlank,
    CellValueNum,
    CellValueMine,
    CellValueTrigger,
    CellValueBase,
} CellValue;

class MinefieldTile
{
public:
    MinefieldTile(const sf::Texture &, int, float, float, CellStatus = CellStatusClose);
    
    void setTexture(const sf::Texture &);
    void setPosition(float, float);
    void setValue(int);
    void setCellStatus(CellStatus);
    void setScale(float, float);
    
    int getValue() const;
    CellStatus getCellStatus() const;
    sf::Vector2f getPosition() const;
    sf::Vector2f getScale() const;
    
    void drawCell(sf::RenderWindow&) const;
    
private:
    int value;
    CellStatus cellStatus;
    sf::Sprite sprite;
    sf::Vector2u size;
};

#endif /* minefieldTile_hpp */
